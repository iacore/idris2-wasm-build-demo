## how to build

1. Install Idris, make sure `idris2` is in `$PATH`
2. Install Zig
3. Run `./build-gmp` (can skip, since the wasm modules are prebuilt in this repo)
4. Set the path to idris source code in `build.zig`. (It's at the top of the file)
5. Run `zig build`
