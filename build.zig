const std = @import("std");

// set your idris source directory here
const IDRIS_SRC = "/home/user/computing/lang/Idris2";
const IDRIS_SUPPORT_SRC = IDRIS_SRC ++ "/support";

pub fn addAllCUnder(b: *std.build.Builder, exe: *std.build.LibExeObjStep, f: []const u8) !void {
    const dir = try std.fs.openIterableDirAbsolute(f, .{});
    var it = dir.iterate();
    while (try it.next()) |child| {
        if (child.kind == .File and std.mem.eql(u8, child.name[child.name.len - 2 .. child.name.len], ".c")) {
            // std.log.info("{}", .{child});
            exe.addCSourceFile(b.fmt("{s}/{s}", .{ f, child.name }), &.{});
        }
    }
}

pub fn build(b: *std.build.Builder) !void {
    const target = b.standardTargetOptions(.{ .default_target = try std.zig.CrossTarget.parse(.{ .arch_os_abi = "wasm32-wasi" }) });
    const mode = b.standardReleaseOptions();

    const compile_idr = b.addSystemCommand(&.{ "idris2", "--cg", "refc", "-o", "hello", "hello.idr" });
    compile_idr.setEnvironmentVariable("IDRIS2_CC", "echo"); // don't let refc backend compile C code (since Zig will do it)

    const exe = b.addExecutable("wasm-idr", "build/exec/hello.c");
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.install();


    exe.step.dependOn(&compile_idr.step);

    exe.linkLibC();
    exe.addIncludePath(IDRIS_SUPPORT_SRC ++ "/c");
    exe.addIncludePath(IDRIS_SUPPORT_SRC ++ "/refc");
    try addAllCUnder(b, exe, IDRIS_SUPPORT_SRC ++ "/c");
    try addAllCUnder(b, exe, IDRIS_SUPPORT_SRC ++ "/refc");
    exe.addIncludePath("gmp-6.2.1");
    exe.addLibraryPath("gmp-6.2.1/.libs");
    exe.linkSystemLibrary("gmp");

    // exe.defineCMacro("_WASI_EMULATED_SIGNAL", "");
    // exe.linkSystemLibrary("wasi-emulated-signal");

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
